package com.entes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class EntesRestApplication {
     public static void main(String[] args) {
    SpringApplication.run(EntesRestApplication.class, args);
  }
}

